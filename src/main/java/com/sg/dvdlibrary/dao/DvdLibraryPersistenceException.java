package com.sg.dvdlibrary.dao;

public class DvdLibraryPersistenceException extends Exception {
    DvdLibraryPersistenceException(String message, Throwable e) {
        super(message, e);
    }
}
