package com.sg.dvdlibrary.dao;

import com.sg.dvdlibrary.dto.Dvd;

import java.io.*;
import java.util.*;

public class DvdLibraryDaoImpl implements DvdLibraryDao {
    private static final String DVD_LIBRARY = "dvdlibrary.txt";
    private static final String DELIMETER = "::";

    @Override
    public void addDvds(List<Dvd> dvdsToAdd) throws DvdLibraryPersistenceException {
        List<Dvd> dvds = getAllDvds();
        dvds.addAll(dvdsToAdd);
        writeDvdLibrary(dvds);
    }

    @Override
    public Dvd getDvd(String title) throws DvdLibraryPersistenceException {
        List<Dvd> dvds = getAllDvds();
        for (Dvd dvd : dvds) {
            if (dvd.getTitle().equalsIgnoreCase(title)) {
                return dvd;
            }
        }
        return null;
    }

    @Override
    public int getIndexOfDvd(Dvd dvdCheck) throws DvdLibraryPersistenceException {
        List<Dvd> dvds = getAllDvds();
        Integer index = -1;
        for (Dvd dvd : dvds) {
            if (areEquivalent(dvdCheck, dvd)) {
                index = dvds.indexOf(dvd);
            }
        }
        return index;
    }

    private boolean areEquivalent(Dvd a, Dvd b) {
        return (a.getTitle().equals(b.getTitle())
                && a.getReleaseDate().equals(b.getReleaseDate())
                && a.getMpaaRating().equals(b.getMpaaRating())
                && a.getDirectorName().equals(b.getDirectorName())
                && a.getStudio().equals(b.getStudio())
                && a.getNotes().equals(b.getNotes()));
    }

    @Override
    public List<Dvd> getAllDvds() throws DvdLibraryPersistenceException {
        return loadDvdLibrary();
    }

    @Override
    public void editDvd(int index, Dvd editedDvd) throws DvdLibraryPersistenceException {
        List<Dvd> dvds = getAllDvds();
        dvds.set(index, editedDvd);
        writeDvdLibrary(dvds);
    }

    @Override
    public void removeDvd(int index) throws DvdLibraryPersistenceException {
        List<Dvd> dvds = getAllDvds();
        dvds.remove(index);
        writeDvdLibrary(dvds);
    }

    private List<Dvd> loadDvdLibrary() throws DvdLibraryPersistenceException {
        Scanner scanner;
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(DVD_LIBRARY)));
        } catch (FileNotFoundException e) {
            throw new DvdLibraryPersistenceException("Unable to load DVD library.", e);
        }

        List<Dvd> dvds = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String[] currentTokens = scanner.nextLine().split(DELIMETER);
            Dvd dvd = new Dvd(currentTokens[0]);
            dvd.setReleaseDate(currentTokens[1]);
            dvd.setMpaaRating(currentTokens[2]);
            dvd.setDirectorName(currentTokens[3]);
            dvd.setStudio(currentTokens[4]);
            dvd.setNotes(currentTokens[5]);
            dvds.add(dvd);
        }
        scanner.close();
        return dvds;
    }

    private void writeDvdLibrary(List<Dvd> dvds) throws DvdLibraryPersistenceException {
        PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter(DVD_LIBRARY));
        } catch (IOException e) {
            throw new DvdLibraryPersistenceException("Could not save DVD information.", e);
        }

        for (Dvd dvd : dvds) {
            out.println(dvd.getTitle() + DELIMETER
                    + dvd.getReleaseDate() + DELIMETER
                    + dvd.getMpaaRating() + DELIMETER
                    + dvd.getDirectorName() + DELIMETER
                    + dvd.getStudio() + DELIMETER
                    + dvd.getNotes() + DELIMETER);
            out.flush();
        }
        out.close();
    }
}
