package com.sg.dvdlibrary.controller;

import com.sg.dvdlibrary.dao.DvdLibraryDao;
import com.sg.dvdlibrary.dao.DvdLibraryPersistenceException;
import com.sg.dvdlibrary.dto.Dvd;
import com.sg.dvdlibrary.ui.DvdLibraryView;

import java.util.ArrayList;
import java.util.List;

public class DvdLibraryController {
    private DvdLibraryView view;
    private DvdLibraryDao dao;

    public DvdLibraryController(DvdLibraryView view, DvdLibraryDao dao) {
        this.view = view;
        this.dao = dao;
    }

    public void openDvdLibrary() {
        boolean run = true;
        try {
            while (run) {
                printMenu();
                int menuSelection = getMenuSelection();
                switch (menuSelection) {
                    case 1:
                        addDvds();
                        break;
                    case 2:
                        findDvd();
                        break;
                    case 3:
                        listAllDvds();
                        break;
                    case 4:
                        editDvd();
                        break;
                    case 5:
                        removeDvd();
                        break;
                    case 0:
                        run = false;
                        printExitMessage();
                        break;
                }
            }
        } catch (DvdLibraryPersistenceException e) {
            view.printErrorMessage(e.getMessage());
        }
    }

    private void printMenu() {
        view.printMenu();
    }

    private int getMenuSelection() {
        return view.getMenuSelection();
    }

    private void addDvds() throws DvdLibraryPersistenceException {
        List<Dvd> dvdsToAdd = new ArrayList<>();
        do {
            Dvd dvd = view.getNewDvdInfo();
            dvdsToAdd.add(dvd);
        } while (view.shouldAddAnother());
        dao.addDvds(dvdsToAdd);
    }

    private void findDvd() throws DvdLibraryPersistenceException {
        view.printFindDvdBanner();
        String title = view.askUserForTitle();
        Dvd dvd = dao.getDvd(title);
        if (dvd != null) {
            view.printDvdInfo(dvd);
        } else {
            view.printDvdNotFound();
        }
        view.userEnterToContinue();
    }

    private void listAllDvds() throws DvdLibraryPersistenceException {
        List<Dvd> dvds = dao.getAllDvds();
        view.listAllDvds(dvds);
        view.userEnterToContinue();
    }

    private void editDvd() throws DvdLibraryPersistenceException {
        do {
            view.printFindDvdBanner();
            String title = view.askUserForTitle();
            Dvd dvd = dao.getDvd(title);
            if (dvd != null) {
                view.printEditDvdBanner();
                view.printDvdInfo(dvd);
                if (view.shouldEdit()) {
                    int index = dao.getIndexOfDvd(dvd);
                    Dvd editedDvd = view.getUpdatedDvdInfo(dvd);
                    dao.editDvd(index, editedDvd);
                    view.printEditSuccessfulMessage();
                } else {
                    view.printEditCancelledMessage();
                }
            } else {
                view.printDvdNotFound();
            }
        } while (view.shouldEditAnother());
    }

    private void removeDvd() throws DvdLibraryPersistenceException {
        do {
            view.printFindDvdBanner();
            String title = view.askUserForTitle();
            Dvd dvd = dao.getDvd(title);
            if (dvd != null) {
                view.printRemoveDvdBanner();
                view.printDvdInfo(dvd);
                if (view.shouldRemove()) {
                    int index = dao.getIndexOfDvd(dvd);
                    dao.removeDvd(index);
                    view.printRemoveSuccessfulMessage();
                } else {
                    view.printRemoveCancelledMessage();
                }
            } else {
                view.printDvdNotFound();
            }
        } while (view.shouldRemoveAnother());
    }

    private void printExitMessage() {
        view.printExitMessage();
    }
}
